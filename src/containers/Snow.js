import React from 'react';
import '../styles/App.sass';
import {connect} from "react-redux"

class Snow extends React.Component {

    constructor(props) {
        super(props)
        var particles = []
        for (var i = 0; i < 40; i++) {
            var step = 0
            while (step < document.documentElement.clientWidth) {
                step += document.documentElement.clientWidth / (this.props.mobile ? 5 : 10) * Math.random()
                particles.push(
                    <circle cx={step} cy="-2%" r={1 + Math.round((this.props.mobile ? 2 : 4) * Math.random())} fill="white" opacity={1 - Math.random() / 2} key={i + "_" + step}>
                        <animate attributeType="XML" attributeName="cy" from="-5%" to="102%" dur="20s" repeatCount="indefinite" begin={`${i*0.5 - Math.random() - 20}s`}></animate>
                    </circle>
                )
            }
        }
        this.state = {
            particles: particles
        }
    }

    render() {
        return (
            <div className="snowWrap">
                <svg width="100%" height="100%">
                    {
                        this.state.particles.map((particle, id) => {
                            return particle
                        })
                    }
                </svg>
            </div>
        );  
    }
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(Snow);
                