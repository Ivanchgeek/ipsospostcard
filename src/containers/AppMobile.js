import React from "react"
import Portrait from "./Portrait"
import App from "./App"

class AppMobile extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            orientation: this.getOritntation()
        }
    }

    handleChange =  () =>  {
        var self = this;
        if ('onorientationchange' in window) {
            window.addEventListener("orientationchange", function() {
                if (self.getOritntation() === "landscape") window.location.reload()
                self.setState({   
                    orientation: self.getOritntation()
                })
            }, false);
        }
    }

    getOritntation = () => {
        return window.orientation === 0 ? "portrait" : "landscape"
    }

    componentDidMount = () => {
        this.handleChange()
    }

    render() {
        var view = <></>
        if (this.state.orientation === "portrait") view = <Portrait/>
        else view = <App mobile/>
        return (
            <>
                {view}
            </>
        );
    }
}

export default AppMobile