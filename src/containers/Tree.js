import React from 'react';
import '../styles/App.sass';
import {connect} from "react-redux"

import tree from "../stuff/Artboard 104iposo ny2020.svg"

const spots = [
    {x: 30, y: 25},
    {x: 40, y: 12},
    {x: 50, y: 25},
    {x: 60, y: 38},
    {x: 39, y: 37},
    {x: 18, y: 37},
    {x: 67, y: 51},
    {x: 33, y: 50},
    {x: 14, y: 51},
    {x: 50, y: 51},
    {x: 3, y: 63},
    {x: -4, y: 76},
    {x: 23, y: 63},
    {x: 41, y: 63.5},
    {x: 59, y: 64},
    {x: 77, y: 65},
    {x: 83, y: 78},
    {x: 66.5, y: 81},
    {x: 49, y: 76},
    {x: 13, y: 75}
]

var freeSpots = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

export const BookSpot = () => {
    var freeIndex= Math.floor(Math.random() * freeSpots.length)
    var freeSpot = freeSpots[freeIndex]
    freeSpots.splice(freeIndex, 1)
    return freeSpot
}

export const GetGlobalSpotPosition = (id, mobile=false) => {
    var spot = spots[id]
    return {
        x: (document.documentElement.clientWidth * (mobile ? 0.6 : 0.5) + (mobile ? 8 : 100) + 
            spot.x * document.getElementById("face").clientHeight * 0.5799901 / 100),
        y: (spot.y / 100  + (mobile ? 0.1 : 0.03)) * document.getElementById("face").clientHeight
    }
}

class Tree extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            marginLeft: 0,
            onePercentX: 0,
            activeBall: -1
        }
    }

    componentDidMount = () => {
        // this.setState({marginLeft:  (document.documentElement.clientWidth * 0.5 - this.refs.face.clientHeight * 0.5799901) / 2})
        this.setState({marginLeft: this.props.mobile ? 10 : 100})
        this.setState({onePercentX:  this.refs.face.clientHeight * 0.5799901 / 100})
    }

    positionHelper = event => {
        if (process.env.NODE_ENV !== 'production') console.log(`x: ${Math.round((event.clientX - document.documentElement.clientWidth * 0.6 - this.state.marginLeft) / this.state.onePercentX)}, y: ${Math.round(event.clientY / document.documentElement.clientHeight * 0.94 * 100)}`)
    }

    idHelper = event => {
        if (process.env.NODE_ENV !== 'production') console.log(event.target.dataset.spot)
    }

    ballClick = event => {
        this.idHelper(event)
        if (this.state.activeBall !== parseInt(event.target.id)) {
            this.setState({activeBall: parseInt(event.target.id)})
        } else {
            this.setState({activeBall: -1})
        }
    }

    render() {
        const {onePercentX, marginLeft, activeBall} = this.state
        return (
            <div className="tree" ref="tree">
                <h1 className="clickOnBalls" style={{opacity: this.props.pins.length === 0 ? "0" : "0.7"}}>Сlick on toys to enlarge</h1>
                <img className="face" src={tree} ref="face" id="face" alt="crissmas tree"></img>
                {this.props.pins.map((pin, id) => {
                    return (
                        <img
                            onClick={this.ballClick}
                            src={pin.src}
                            className="ball"
                            alt="snow ball"
                            style={{
                                left: `${marginLeft + spots[pin.spot].x * onePercentX}px`,
                                top: spots[pin.spot].y + "%",
                                transform: `scale(${activeBall === id ? this.props.mobile ? "4.5" : "3.5" : "1"})`,
                                zIndex: activeBall === id ? "5" : ""
                            }}
                            key={id}
                            id={id}
                            data-spot={pin.spot}
                        ></img>
                    )
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    pins: state.tree.pins
});
export default connect(mapStateToProps)(Tree);
                