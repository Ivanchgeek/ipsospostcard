import React from 'react';
import '../styles/App.sass';
import {connect} from "react-redux"
import box from "../stuff/Artboard 22 copyiposo ny2020.svg"

class Window extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            opened: false,
            ballUp: false,
            move: false,
            realBallHidden: false
        }
    }

    open = event => {
        if (!this.props.lock) {
            this.props.dispatch({type: "LockWindows"})
            this.props.dispatch({type: "SetBallInProccess", value: true})
            this.setState({opened: true})
            setTimeout(() => {
                this.setState({ballUp: true})
                setTimeout(() => {
                    this.props.dispatch({type: "SpawnBall", x: this.props.x, y: this.props.y, src: this.props.ball, mobile: this.props.mobile})
                    this.setState({realBallHidden: true})
                    setTimeout(() => {
                        this.props.dispatch({type: "CloseUpBall"})
                        setTimeout(() => {
                            this.props.dispatch({type: "SetBallInProccess", value: false})
                        }, 800)
                    }, 1)
                }, 50)
            }, 300)
        }
    }

    render() {
        var position = {
            top: `calc((90vh / 5 * ${this.props.y} )`,
            right: `calc(90vh / 5 * ${this.props.x} + 10px)`
        }
        var curtainStyle = { 
            ...position,
            animation: this.state.opened ? "open .8s forwards" : ""
        }
        var ballStyle = {
            ...position,
            top: this.state.move ? "10%" : position.top,
            left: this.state.move ? "80%" : position.left,
            transform: this.state.move ? "translateX(-50%)" : "",
            height: this.state.move ? "70%" : "",
            zIndex: this.state.ballUp ? "12" : "",
            display: this.state.realBallHidden ? "none" : ""
        }
        return (
            <>
                <img 
                    src={this.props.src} 
                    className="window"
                    style={curtainStyle}
                    onClick={this.open}
                    alt="window svg"
                ></img>
                <img 
                    src={box} 
                    className="window substrate"
                    style={position}
                    alt="window substrate svg"
                ></img>
                <img 
                    src={this.props.ball} 
                    className="window ball"
                    style={ballStyle}
                    alt="snow ball"
                ></img>
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    lock: state.windows.lock
});
export default connect(mapStateToProps)(Window);
