import React from "react"
import rotateIco from "../stuff/rotate_ico.png"

class Portrait extends React.Component {
    render() {
        return (
            <div className="portrait">
                <img src={rotateIco} alt="change device orientation please"></img>
            </div>
        );
    }
}

export default Portrait