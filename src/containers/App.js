import React from 'react';
import "../styles/App.sass"
import Window from "./Window"
import Snow from "./Snow"
import Tree from "./Tree"
import {windowStack, rawBallStack} from "../sources/sources"
import {CSSTransition} from "react-transition-group"
import {connect} from "react-redux"
import logo from "../stuff/logo.png"
import mute from "../stuff/mute.svg"
import unmute from "../stuff/unmute.svg"

class App extends React.Component {

  constructor(props) {
    super(props)
    var raw = rawBallStack
    var ballStack = []
    for (var x = 0; x < 4; x++) {
      var row = []
      for (var y = 0; y < 5; y++) {
        var index = Math.floor(Math.random() * raw.length)
        row.push(raw[index])
        raw.splice(index, 1)
      }
      ballStack.push(row)
    }
    this.state = {
      ballStack: ballStack
    }
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.props.dispatch({type: "SetHiAnim", value: true})
    }, 500)
    setTimeout(() => {
      this.props.dispatch({type: "CloseHi"})
      setTimeout(() => {
        this.props.dispatch({type: "SetHiAnim", value: false})
      }, 1000)
    }, 3000)
  }

  curtainClick = () => {
    if (this.props.hi) {
      this.props.dispatch({type: "CloseHi"})
      setTimeout(() => {
        this.props.dispatch({type: "SetHiAnim", value: false})
      }, 1000)
      return
    }
    if (!this.props.ballProccessing) {
      this.props.dispatch({type: "SetBallInProccess", value: true})
      this.props.dispatch({type: "AimBall", mobile: this.props.mobile})
      setTimeout(() => {
        this.props.dispatch({type: "PinBall", src: this.props.ballSrc, spot: this.props.spot})
        this.props.dispatch({type: "HideBall"})
        this.props.dispatch({type: "UnlockWindows"})
        setTimeout(() => {
          this.props.dispatch({type: "SetBallInProccess", value: false})
        }, 300)
      }, 800)
    }
  }

  render() {
    return (
      <div className={`wrap ${this.props.mobile ? "mobile" : ""}`}>
        <img src={this.props.musicState ? mute : unmute} className="mute" onClick={() => {this.props.dispatch({type: "SwitchMusic"})}} alt="mute button"></img>
        <p className="mark">Optimized for Google Chrome</p>
        <CSSTransition
          in = {this.props.curtained || this.props.hi || this.props.bye}
          timeout={{ enter: this.props.hiAnim ? 800 : 500, exit: this.props.hiAnim ? 800 : 500 }}
          classNames={this.props.bye || this.props.hiAnim ? "bump" : "fade"}
          appear>
              <div className={`curtain ${this.props.hi ? "hi" : ""} ${this.props.bye ? "bye" : ""}`} onClick={this.curtainClick}>
                {this.props.hi || this.props.hiAnim || this.props.bye ? <img className="logo" src={logo} alt="logo"></img> : <></>}
                {this.props.hi || this.props.hiAnim ? <h1>SEASON'S<br/>GREETINGS</h1> : <></>}
                {this.props.bye? <h1>WE WISH YOU<br/>A TRENDY YEAR<br/>2020!</h1> : <></>}
                {this.props.bye ? <button onClick={() => {window.location.reload()}} style={{color: "white"}} className="exit">restart</button> : <></>}
              </div>
        </CSSTransition>
        {
          (() => {
            if (this.props.showBall) {
              return (
                <img 
                    src={this.props.ballSrc} 
                    className="pseudoball"
                    style={this.props.ballStyle}
                    alt="snow ball"
                ></img>
              );
            } else {
              return <></>
            }
          })()
        }
        <div className="app">
            <h1 className="tryto">Click on windows</h1>
            <div className="house">
              <h1 alt="crismass" className="puzle">WE WISH YOU A TRENDY YEAR 2020!</h1>
              <img alt="logo" className="logo" src={logo}></img>
              {windowStack.map((row, y) => {
                return (
                  row.map((src, x) => {
                    return (
                      <Window x={x} y={y} src={src} ball={this.state.ballStack[x][y]} key={`${x}_${y}`} mobile={this.props.mobile}></Window>
                    )
                  })
                )
              })}
            </div>
            <Tree mobile={this.props.mobile}></Tree>
        </div>
        <Snow mobile={this.props.mobile}></Snow>
        <button onClick={() => {this.props.dispatch({type: "OpenBye"})}} className="exit">exit</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  curtained: state.share.curtained,
  ballSrc: state.share.ballSrc,
  ballStyle: state.share.ballStyle,
  showBall: state.share.showBall,
  spot: state.share.spot,
  hi: state.share.hi,
  hiAnim: state.share.hiAnim,
  bye: state.share.bye,
  ballProccessing: state.share.ballInProccess,
  musicState: state.share.musicState
});
export default connect(mapStateToProps)(App);
