import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import AppMobile from './containers/AppMobile';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import {BrowserView, MobileView} from "react-device-detect";

const store = configureStore()

console.info("___________")
console.info("Powered by Ivanchgeek")
console.info("contact: iam@ivanchgeek.ru")
console.info("___________")

ReactDOM.render(
    <Provider store={store}>
        <BrowserView><App/></BrowserView>
        <MobileView><AppMobile/></MobileView>
    </Provider>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
