import { combineReducers } from 'redux'

import share from "./share"
import tree from "./tree"
import windows from "./windows"

export default combineReducers({
    share,
    tree, 
    windows
})
