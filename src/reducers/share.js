import {BookSpot, GetGlobalSpotPosition} from "../containers/Tree"
import music1 from "../stuff/music.mp3"
import music2 from "../stuff/music2.mp3"
import music3 from "../stuff/music3.mp3"

var music

const initialState = {
    curtained: false,
    showBall: false,
    ballStyle: {},
    ballSrc: "", 
    spot: 0,
    hi: true,
    bye: false,
    ballInProccess: false,
    audio: false,
    hiAnim: false,
    musicState: true
}

export default function share(state = initialState, action) {
    switch (action.type) {
        case "SetCurtained":
                return ({
                    ...state,
                    curtained: action.value
                })
        case "SpawnBall": 
            if (!state.audio && state.musicState) startMusic()
            var ballStyle = {
                top: `calc((90vh / 5 * ${action.y } + 5vh)`,
                left: `calc(${action.mobile ? 60 : 50}vw - 90vh / 5 * ${action.x + 1} - 10px)`
            }
            return({
                ...state,
                showBall: true,
                ballStyle: ballStyle,
                ballSrc: action.src,
                audio: true
            })
        case "CloseUpBall":
            return({
                ...state,
                ballStyle: {
                    top: "20%",
                    left: "50%",
                    margin: "0",
                    height: "60%",
                    transform: "translateX(-50%)" 
                },
                curtained: true
            })
        case "AimBall":
            var spot = BookSpot()
            var pos = GetGlobalSpotPosition(spot, action.mobile)
            return({
                ...state,
                ballStyle: {
                    top: pos.y + "px",
                    left: pos.x + "px",
                    height: `calc( ${action.mobile ? 80 : 94}vh * 0.5799901 / 6 * 1.25)`,
                    transform: "translate(-50%, -40%)" 
                },
                curtained: false,
                spot: spot
            })
        case "HideBall":
            return({
                ...state,
                showBall: false
            })
        case "CloseHi":
            return({
                ...state,
                hi: false
            })
        case "OpenBye":
            return({
                ...state,
                bye: true
            })
        case "SetBallInProccess":
            return({
                ...state,
                ballInProccess: action.value
            })
        case "SetHiAnim": 
            return({
                ...state,
                hiAnim: action.value
            })
        case "SwitchMusic": 
            if (music !== undefined) {
                if (state.musicState) {
                    music.pause()
                } else {
                    music.play()
                }   
            }
            return({
                ...state,
                musicState: !state.musicState
            })
        default:
            return state
    }
}

const musicStack = [music1, music2, music3]

function startMusic(id = 0) {
    music = new Audio()
    if (id === 0) music.play().catch((e) => {})
    music.src = musicStack[id]
    music.load()
    music.autoplay = true
    music.play()
    music.onended = () => {
      id++
      startMusic(id >= musicStack.length ? 0 : id)
    }
  }
