const initialState = {
    pins: []
}

export default function share(state = initialState, action) {
    switch (action.type) {
        case "PinBall":
            return ({
                ...state,
                pins: [...state.pins, {
                    src: action.src,
                    spot: action.spot
                }]
            })
        default:
            return state
    }
}
