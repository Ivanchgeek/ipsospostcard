const initialState = {
    lock: false
}

export default function windows(state = initialState, action) {
    switch (action.type) {
        case "LockWindows":
            return ({
                ...state,
                lock: true
            })
        case "UnlockWindows":
            return ({
                ...state,
                lock: false
            })
        default:
            return state
    }
}
