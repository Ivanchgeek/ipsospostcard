import window_1 from "../stuff/wnd_21.svg"
import window_2 from "../stuff/wnd_22.svg"
import window_3 from "../stuff/wnd_23.svg"
import window_4 from "../stuff/wnd_24.svg"
import window_5 from "../stuff/wnd_27.svg"
import window_6 from "../stuff/wnd_28.svg"
import window_7 from "../stuff/wnd_29.svg"
import window_8 from "../stuff/wnd_30.svg"
import window_9 from "../stuff/wnd_31.svg"
import window_10 from "../stuff/wnd_32.svg"
import window_11 from "../stuff/wnd_33.svg"
import window_12 from "../stuff/wnd_34.svg"
import window_13 from "../stuff/wnd_36.svg"
import window_14 from "../stuff/wnd_37.svg"
import window_15 from "../stuff/wnd_38.svg"
import window_16 from "../stuff/wnd_39.svg"
import window_17 from "../stuff/wnd_40.svg"
import window_18 from "../stuff/wnd_41.svg"
import window_19 from "../stuff/wnd_43.svg"
import window_20 from "../stuff/wnd_44.svg"

import ball_1 from "../stuff/Artboard 60iposo ny2020.svg"
import ball_2 from "../stuff/Artboard 61iposo ny2020.svg"
import ball_3 from "../stuff/Artboard 62iposo ny2020.svg"
import ball_4 from "../stuff/Artboard 63iposo ny2020.svg"
import ball_5 from "../stuff/Artboard 64iposo ny2020.svg"
import ball_6 from "../stuff/Artboard 65iposo ny2020.svg"
import ball_7 from "../stuff/Artboard 66iposo ny2020.svg"
import ball_8 from "../stuff/Artboard 67iposo ny2020.svg"
import ball_9 from "../stuff/Artboard 68iposo ny2020.svg"
import ball_10 from "../stuff/Artboard 69iposo ny2020.svg"
import ball_11 from "../stuff/Artboard 71iposo ny2020.svg"
import ball_12 from "../stuff/Artboard 72iposo ny2020.svg"
import ball_13 from "../stuff/Artboard 73iposo ny2020.svg"
import ball_14 from "../stuff/Artboard 74iposo ny2020.svg"
import ball_15 from "../stuff/Artboard 75iposo ny2020.svg"
import ball_16 from "../stuff/Artboard 76iposo ny2020.svg"
import ball_17 from "../stuff/Artboard 77iposo ny2020.svg"
import ball_18 from "../stuff/Artboard 78iposo ny2020.svg"
import ball_19 from "../stuff/Artboard 79iposo ny2020.svg"
import ball_20 from "../stuff/Artboard 80iposo ny2020.svg"

export const windowStack = [
    [window_1, window_2, window_3, window_4],
    [window_5, window_6, window_7, window_8],
    [window_9, window_10, window_11, window_12],
    [window_13, window_14, window_15, window_16],
    [window_17, window_18, window_19, window_20]
]

export var rawBallStack = [
    ball_1, ball_2, ball_3, ball_4, ball_5, ball_6, ball_7, ball_8, ball_9, ball_10,
    ball_11, ball_12, ball_13, ball_14, ball_15, ball_16, ball_17, ball_18, ball_19, ball_20
]